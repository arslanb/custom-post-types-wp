<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Custom_Post_Types
 * @subpackage Custom_Post_Types/admin/partials
 */
?>

<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	<form action="" method="post">
		<table cellspacing="0" cellpadding="0" class="form-table" >
			<tr>
				<th>
					<label for="cpPostTypeSlug">Post Type Slug <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_post_type_slug" id="cpPostTypeSlug" required="true"/></td>
			</tr>
			<tr>
				<th>
					<label for="cpPostPluralLabel">Plural Label <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_post_plural_label" id="cpPostPluralLabel" required="true"/></td>
			</tr>
			<tr>
				<th>
					<label for="cpPostSingularLabel">Singular Label <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_post_singular_label" id="cpPostSingularLabel" required="true"/></td>
			</tr>
		</table>
		<?php wp_nonce_field( 'cp_addedit_post_type_nonce_action', 'cp_addedit_post_type_nonce_field' ); ?>
		<input name="form-post-type" value="create-post-type" type="hidden"/>
		<p class="submit"><input class="button button-primary" name="create" value="Create Post Type" type="submit"/></p>
	</form>
</div>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
