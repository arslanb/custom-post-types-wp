<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Custom_Post_Types
 * @subpackage Custom_Post_Types/admin/partials
 */
?>
<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
	<form action="" id="addTaxonomyForm" method="post">
		<table cellspacing="0" cellpadding="0" class="form-table" >
			<tr>
				<th>
					<label for="cpTaxonomySlug">Taxonomy Slug <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_taxonomy_slug" id="cpTaxonomySlug" required="true"/></td>
			</tr>
			<tr>
				<th>
					<label for="cpTaxonomyPluralLabel">Plural Label <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_taxonomy_plural_label" id="cpTaxonomyPluralLabel" required="true"/></td>
			</tr>
			<tr>
				<th>
					<label for="cpTaxonomySingularLabel">Singular Label <span class="required">*</span></label>
				</th>
				<td><input aria-required="true" name="cp_taxonomy_singular_label" id="cpTaxonomySingularLabel" required="true"/></td>
			</tr>
			<tr>
				<td>
					<label>Attach to Post Type <span class="required">*</span></label>
				</td>
				<td>
					<fieldset tabindex="0" style="border: 1px solid #ccc; display: block; margin-bottom: 30px; padding: 10px; overflow: hidden;">
						<?php
							$args = array( 'public' => true );
							$post_types = get_post_types($args, 'objects');
							foreach($post_types as $post_type) {
								$label = (in_array($post_type->name, array('post', 'page', 'attachment')))? $post_type->label.' (WP Core)': $post_type->label;
								echo '<input id="'.$post_type->name.'" name="cp_post_types[]" value="'.$post_type->name.'" type="checkbox"/><label for="'.$post_type->name.'">'.$label.'</label><br />';
							}
						?>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php wp_nonce_field( 'cp_add_taxonomy_nonce_field', 'cp_add_taxonomy_nonce_field' ); ?>
		<input name="form-taxonomy" value="create-taxonomy" type="hidden"/>
		<p class="submit"><input class="button button-primary" name="create" value="Create Taxonomy" type="submit"/></p>
	</form>
</div>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
