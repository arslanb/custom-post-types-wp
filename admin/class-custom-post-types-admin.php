<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Custom_Post_Types
 * @subpackage Custom_Post_Types/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Post_Types
 * @subpackage Custom_Post_Types/admin
 * @author     Arslan B <arslan.b@allshoreresources.com>
 */
class Custom_Post_Types_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    Custom Post Types    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Post_Types_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Post_Types_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-post-types-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Post_Types_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Post_Types_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-post-types-admin.js', array( 'jquery' ), time(), false );

	}

	/**
	 * This function add the plugin menu item to admin menu bar
	 */
	public function add_plugin_menu_item() {

		$capability = apply_filters( 'cp_required_capabilities', 'manage_options' );
		$parent_slug = 'cp_main_menu';

		add_menu_page( __( 'Custom Post Types', 'cp' ), __( 'Custom Posts UI', 'cp' ), $capability, $parent_slug, 'cp_main_menu');
		add_submenu_page( $parent_slug, __( 'Add Post Types', 'cp' ), __( 'Add Post Types', 'cp' ), $capability, 'create_post_types_UI', array($this, 'create_post_types_UI') );
		add_submenu_page( $parent_slug, __( 'Add Taxonomies', 'cp' ), __( 'Add Taxonomies', 'cp' ), $capability, 'create_custom_taxonomy_UI', array($this, 'create_custom_taxonomy_UI') );

		// Remove the default menu item that is parent item
		remove_submenu_page( $parent_slug, 'cp_main_menu' );

	}

	/**
	 * Display the Add Post Type form and handle the request of the form
	 */

	public function create_post_types_UI(){

		$post_type_request = $_POST['form-post-type'];
		if(!empty($post_type_request)){

			$data = array(
				'slug'      =>  strtolower($_POST['cp_post_type_slug']),
				'plural'    =>  ucwords($_POST['cp_post_plural_label']),
				'singular'  =>  ucwords($_POST['cp_post_singular_label'])
			);

			$post_type_data = get_option('cp_post_types');

			$post_type_data[$data['slug']] = $data;
			update_option('cp_post_types', $post_type_data);
			echo '<script>window.location = "'.$_SERVER['HTTP_REFERER'].'";</script>';

		}

		include_once __DIR__.'/partials/custom-post-types-admin-display.php';
	}


	/**
	 * Register Post Types in WP
	 */
	public function create_post_type(){

		$post_type_data = get_option('cp_post_types');
		if(empty($post_type_data)){
			return false;
		}

		foreach($post_type_data as $post_type){

			$slug = isset($post_type['slug'])? $post_type['slug']:'';
			$plural = isset($post_type['plural'])? $post_type['plural']:'Post Types';
			$singular = isset($post_type['singular'])? $post_type['singular']:'Post Type';

			$labels = array(
				'name'                  => _x( $plural, 'Post Type General Name', 'cp' ),
				'singular_name'         => _x( $singular, 'Post Type Singular Name', 'cp' ),
				'all_items'             => __( 'All '.$plural, 'cp' ),
			);


			$args = array(
				'label'                 => __( $plural, 'cp' ),
				'description'           => '',
				'labels'                => $labels,
				'supports'              => array( ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_position'         => 5,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => false,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'post',
			);
			register_post_type( $slug, $args );
		}
	}

	/**
	 * Display Add Taxonomy Form and handle form submission request
	 */

	public function create_custom_taxonomy_UI(){

		$taxonomy_form_request = $_POST['form-taxonomy'];
		if(!empty($taxonomy_form_request)){
			$post_types = $_POST['cp_post_types'];

			if(!empty($post_types)){
				$data = array(
					"slug" => strtolower($_POST['cp_taxonomy_slug']),
					"singular_name" => ucwords($_POST['cp_taxonomy_singular_label']),
					"plural_name" => ucwords($_POST['cp_taxonomy_plural_label']),
					'post_types'    => $_POST['cp_post_types']
				);
				$taxonomies_data = get_option('cp_taxonomies');
				$taxonomies_data[$data['slug']] = $data;
				update_option('cp_taxonomies', $taxonomies_data);
				echo '<script>window.location = "'.$_SERVER['HTTP_REFERER'].'";</script>';
			}

		}
		include_once __DIR__.'/partials/custom-post-types-admin-display-taxonomy.php';
	}

	/**
	 * Register Custom Taxonomies in WP
	 *
	 */

	public function create_custom_taxonomy(){

		$taxonomy_data = get_option('cp_taxonomies');
		if(empty($taxonomy_data)){
			return false;
		}

		foreach($taxonomy_data as $taxonomy){

			$slug = isset($taxonomy['slug'])? $taxonomy['slug']:'';
			$plural = isset($taxonomy['plural_name'])? ucwords($taxonomy['plural_name']):'';
			$singular = isset($taxonomy['singular_name'])? ucwords($taxonomy['singular_name']):'';

			$labels = array(
				'name'                  => __( $plural, 'cp' ),
				'singular_name'         => __( $singular, 'cp' ),
			);

			$args = array(
				"label" => __( $plural, "cp" ),
				"labels" => $labels,
				"public" => true,
				"hierarchical" => false,
				"label" => $plural,
				"show_ui" => true,
				"show_in_menu" => true,
				"show_in_nav_menus" => true,
				"query_var" => true,
				"rewrite" => array( 'slug' => $slug, 'with_front' => true, ),
				"show_admin_column" => false,
				"show_in_rest" => false,
				"rest_base" => "",
				"show_in_quick_edit" => false,
			);

			register_taxonomy( $slug, $taxonomy['post_types'], $args );
		}
	}

}
